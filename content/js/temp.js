var query = "";
var category = "<?=$c?>";
var search = $("input[class='search']");
if (query !== "") search.val(query);
search.keyup(function(e) {
    /*if (e.keyCode == 13) {
        var url = "http://hexbot.org/store/";
        if (category !== "") url += category;
        url += "?q=" + $(this).val();
        window.location.href = url;
    }*/
});
function addScriptHandlers() {
    var i = 0;
    var basics = $("div[class='basic_script']");
    var basic_imgs = $("div[class='basic_script_img']");
    var stars = $("img[class='star']");
    var ratings = $("div[class='basic_script_info_rating']");
    var basic_btns = $("div[class='basic_script_info_button']");
    basics.each(function() {
        i++;
        if (i % 3 == 0) $(this).css("border-right", "none");
    });
    basic_imgs.each(function() {
        $(this).unbind("hover");
    });
    basic_imgs.hover(function() {
        var parent = $(this).parent();
        parent.slideUp(150, function() {
            $(parent.parent()).find("div[class='basic_script_info']").fadeIn();
        });
    });
    stars.each(function() {
        $(this).unbind("hover");
        $(this).unbind("click");
    });
    stars.hover(function() {
        var rating = $(this).attr("id");
        var idx = 0;
        for (var i = 0; i < rating; i++) {
            $(this).parent().children().eq(idx).attr("src", "/store/images/star_yellow.png");
            idx++;
        }
        for (var i = 0; i < 5 - rating; i++) {
            $(this).parent().children().eq(idx).attr("src", "/store/images/star_gray.png");
            idx++;
        }
    });
    ratings.each(function() {
        $(this).unbind("hover");
    });
    ratings.hover(null, function() {
        var rating = $(this).attr("id");
        var idx = 0;
        for (var i = 0; i < rating; i++) {
            $(this).children().eq(idx).attr("src", "/store/images/star_yellow.png");
            idx++;
        }
        for (var i = 0; i < 5 - rating; i++) {
            $(this).children().eq(idx).attr("src", "/store/images/star_gray.png");
            idx++;
        }
    });
    stars.click(function() {
        var parent = $(this).parent();
        var id = $(this).parent().parent().parent().attr("id");
        $.post("script_action.php", {sid: id, action: 3, stars: $(this).attr("id")}, function(data) {
            if (data != null) {
                if (data.result != null && data.result === "Success") {
                    var offset = parent.offset();
                    var thanks = $("div#thanks");
                    thanks.css("display", "block");
                    thanks.offset({ top: offset.top, left: offset.left });
                    thanks.fadeOut(5000, function() {
                        thanks.css("display", "none");
                    });
                    var rating = data.msg;
                    var idx = 0;
                    for (var i = 0; i < rating; i++) {
                        parent.children().eq(idx).attr("src", "/store/images/star_yellow.png");
                        idx++;
                    }
                    for (var i = 0; i < 5 - rating; i++) {
                        parent.children().eq(idx).attr("src", "/store/images/star_gray.png");
                        idx++;
                    }
                } else {
                    alert(data.msg);
                }
            }
        }, "json");
    });
    basic_btns.each(function() {
        $(this).unbind("click");
    });
    basic_btns.click(function() {
        var btn = $(this);
        var id = $(this).attr("id");
        var txt = $(this).html();
        if (txt == "ADD" || txt.indexOf("HP") !== -1) {
            $.post("script_action.php", {sid : id, action : 0}, function(data) {
                if (data != null && data.result != null) {
                    if (data.result == "Success") {
                        btn.css("background", "#FF3333");
                        btn.html("REMOVE");
                        if (txt.indexOf("HP") !== -1) window.location.reload();
                    } else {
                        if (data.msg != null) alert(data.msg);
                    }
                }
            }, "json");
        } else if ($(this).html() == "REMOVE") {
            $.post("script_action.php", {sid : id, action : 1}, function(data) {
                if (data != null && data.result != null) {
                    if (data.result == "Success") {
                        btn.css("background", "#58ACFA");
                        btn.html("ADD");
                    } else {
                        if (data.msg != null) alert(data.msg);
                    }
                }
            }, "json");
        } else if ($(this).html() === "VIP+") {
            //redirect to buy vip
        } else if ($(this).html() === "Guest") {
            //redirect to login
            window.location = "/forum/index.php?app=core&module=global&section=login";
        }
    });
}
addScriptHandlers();
window.onload = function start() {
    setTimeout(check, 1000);
};
function check() {
    $("div[class='basic_script_info']").each(function() {
        if (!$(this).is(":hidden")) {
            if (!$(this).is(":hover")) {
                $(this).fadeOut(function() {
                    $(this).parent().find("div[class='basic_script_display']").slideDown(250);
                });
            }
        }
    });
    setTimeout(check, 1000);
}
var categories = $("div[class='category']");
if (category) {
    categories.each(function() {
        if ($(this).html().toLowerCase() === category.toLowerCase()) {
            $("span[id='header']").html($(this).html());
            $(this).css({"background":"#5080D8", "color":"#FFFFFF"});
        }
    });
} else {
    if (query !== "") {
        $("span[id='header']").html("Results");
    } else {
        var author = "<?=$author?>";
        if (author !== "") {
            var author_name = "<?php echo name_for(intval($author)) ?>";
            $("span[id='header']").html("Scripts by " + author_name);
        } else {
            $("span[id='header']").html("Popular");
            categories.eq(3).css({"background":"#5080D8", "color":"#FFFFFF"});
        }
    }
}
$("div[class='categories']").css("height", ($(window).height() - 100) + "px");
$(window).resize(function() {
    $("div[class='categories']").css("height", (Math.max(100, $(window).height() - 100)) + "px");
});
$(document).ready(function() {
    $(this).scrollTop(0);
    $('html').animate({scrollTop:0}, 1);
    $('body').animate({scrollTop:0}, 1);
    var offset = 24;
    var scroll = true;
    var time = -1;
    var submitOnScroll = function() {
        if (time == -1 || new Date().getTime() - time > 5000) {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 150) {
                $.ajax({
                    url: "index.php",
                    type: "POST",
                    data: {
                        o: offset,
                        c: "<?=$c?>",
                        q: "<?=$query?>",
                        a: "<?=$author?>"
                    },
                    beforeSend: function() {
                        $(window).unbind("scroll", submitOnScroll);
                        $('#loading').css("display", "block");
                    },
                    success: function(data) {
                        if (data) {
                            $('#standard').append(data);
                            offset += 24;
                            if ($("div[class='msg']") !== undefined) {
                                scroll = false;
                            }
                            addScriptHandlers();
                        }
                    },
                    complete: function() {
                        $('#loading').css("display", "none");
                        if (scroll) {
                            $(window).bind("scroll", submitOnScroll);
                        }
                        time = new Date().getTime();
                    },
                    error: function() {
                        $("#error").css("display", "block");
                        $('#error').fadeOut(5000, function() {
                            $("#error").css("display", "none");
                        });
                    }
                });
            }
        }
    };
    if ($("div[class='msg']") === undefined) {
        $(window).scroll(submitOnScroll);
    }
});
(function(e){jQuery.fn.extend({slimScroll:function(n){var r={width:"auto",height:"250px",size:"7px",color:"#000",position:"right",distance:"1px",start:"top",opacity:.4,alwaysVisible:false,disableFadeOut:false,railVisible:false,railColor:"#333",railOpacity:.2,railDraggable:true,railClass:"slimScrollRail",barClass:"slimScrollBar",wrapperClass:"slimScrollDiv",allowPageScroll:false,wheelStep:20,touchScrollStep:200,borderRadius:"7px",railBorderRadius:"7px"};var i=e.extend(r,n);this.each(function(){function x(t){if(!r){return}var t=t||window.event;var n=0;if(t.wheelDelta){n=-t.wheelDelta/120}if(t.detail){n=t.detail/3}var s=t.target||t.srcTarget||t.srcElement;if(e(s).closest("."+i.wrapperClass).is(m.parent())){T(n,true)}if(t.preventDefault&&!v){t.preventDefault()}if(!v){t.returnValue=false}}function T(e,t,n){v=false;var r=e;var s=m.outerHeight()-E.outerHeight();if(t){r=parseInt(E.css("top"))+e*parseInt(i.wheelStep)/100*E.outerHeight();r=Math.min(Math.max(r,0),s);r=e>0?Math.ceil(r):Math.floor(r);E.css({top:r+"px"})}c=parseInt(E.css("top"))/(m.outerHeight()-E.outerHeight());r=c*(m[0].scrollHeight-m.outerHeight());if(n){r=e;var u=r/m[0].scrollHeight*m.outerHeight();u=Math.min(Math.max(u,0),s);E.css({top:u+"px"})}m.scrollTop(r);m.trigger("slimscrolling",~~r);k();L()}function N(){if(window.addEventListener){this.addEventListener("DOMMouseScroll",x,false);this.addEventListener("mousewheel",x,false);this.addEventListener("MozMousePixelScroll",x,false)}else{document.attachEvent("onmousewheel",x)}}function C(){l=Math.max(m.outerHeight()/m[0].scrollHeight*m.outerHeight(),d);E.css({height:l+"px"});var e=l==m.outerHeight()?"none":"block";E.css({display:e})}function k(){C();clearTimeout(a);if(c==~~c){v=i.allowPageScroll;if(h!=c){var e=~~c==0?"top":"bottom";m.trigger("slimscroll",e)}}else{v=false}h=c;if(l>=m.outerHeight()){v=true;return}E.stop(true,true).fadeIn("fast");if(i.railVisible){w.stop(true,true).fadeIn("fast")}}function L(){if(!i.alwaysVisible){a=setTimeout(function(){if(!(i.disableFadeOut&&r)&&!s&&!u){E.fadeOut("slow");w.fadeOut("slow")}},1e3)}}var r,s,u,a,f,l,c,h,p="<div></div>",d=30,v=false;var m=e(this);if(m.parent().hasClass(i.wrapperClass)){var g=m.scrollTop();E=m.parent().find("."+i.barClass);w=m.parent().find("."+i.railClass);C();if(e.isPlainObject(n)){if("height"in n&&n.height=="auto"){m.parent().css("height","auto");m.css("height","auto");var y=m.parent().parent().height();m.parent().css("height",y);m.css("height",y)}if("scrollTo"in n){g=parseInt(i.scrollTo)}else if("scrollBy"in n){g+=parseInt(i.scrollBy)}else if("destroy"in n){E.remove();w.remove();m.unwrap();return}T(g,false,true)}return}i.height=i.height=="auto"?m.parent().height():i.height;var b=e(p).addClass(i.wrapperClass).css({position:"relative",overflow:"hidden",width:i.width,height:i.height});m.css({overflow:"hidden",width:i.width,height:i.height});var w=e(p).addClass(i.railClass).css({width:i.size,height:"100%",position:"absolute",top:0,display:i.alwaysVisible&&i.railVisible?"block":"none","border-radius":i.railBorderRadius,background:i.railColor,opacity:i.railOpacity,zIndex:90});var E=e(p).addClass(i.barClass).css({background:i.color,width:i.size,position:"absolute",top:0,opacity:i.opacity,display:i.alwaysVisible?"block":"none","border-radius":i.borderRadius,BorderRadius:i.borderRadius,MozBorderRadius:i.borderRadius,WebkitBorderRadius:i.borderRadius,zIndex:99});var S=i.position=="right"?{right:i.distance}:{left:i.distance};w.css(S);E.css(S);m.wrap(b);m.parent().append(E);m.parent().append(w);if(i.railDraggable){E.bind("mousedown",function(n){var r=e(document);u=true;t=parseFloat(E.css("top"));pageY=n.pageY;r.bind("mousemove.slimscroll",function(e){currTop=t+e.pageY-pageY;E.css("top",currTop);T(0,E.position().top,false)});r.bind("mouseup.slimscroll",function(e){u=false;L();r.unbind(".slimscroll")});return false}).bind("selectstart.slimscroll",function(e){e.stopPropagation();e.preventDefault();return false})}w.hover(function(){k()},function(){L()});E.hover(function(){s=true},function(){s=false});m.hover(function(){r=true;k();L()},function(){r=false;L()});m.bind("touchstart",function(e,t){if(e.originalEvent.touches.length){f=e.originalEvent.touches[0].pageY}});m.bind("touchmove",function(e){if(!v){e.originalEvent.preventDefault()}if(e.originalEvent.touches.length){var t=(f-e.originalEvent.touches[0].pageY)/i.touchScrollStep;T(t,true);f=e.originalEvent.touches[0].pageY}});C();if(i.start==="bottom"){E.css({top:m.outerHeight()-E.outerHeight()});T(0,true)}else if(i.start!=="top"){T(e(i.start).position().top,null,true);if(!i.alwaysVisible){E.hide()}}N()});return this}});jQuery.fn.extend({slimscroll:jQuery.fn.slimScroll})})(jQuery)