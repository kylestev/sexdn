<?php
/**
 * Created by JetBrains PhpStorm.
 * User: kyle
 * Date: 11/30/13
 * Time: 5:55 PM
 * To change this template use File | Settings | File Templates.
 */

namespace HexbotRepository;


use Illuminate\Support\Facades\DB;
use User;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserProviderInterface;

class UserAuthProvider implements UserProviderInterface {
    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Auth\UserInterface|null
     */
    public function retrieveById($identifier) {
        return User::find($identifier);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Auth\UserInterface|null
     */
    public function retrieveByCredentials(array $credentials) {
        $user = DB::table('members')
                ->where('name', $credentials['username'])
                ->first();

        $real = User::find($user->member_id);

        if (!$this->validateCredentials($real, $credentials)) {
            return null;
        }

        return $real;
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Auth\UserInterface $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(UserInterface $user, array $credentials) {
        $hashed_pw = md5($credentials['password']);
        /** @noinspection PhpUndefinedMethodInspection */
        $hashed_salted_pw = md5($user->getPasswordSalt().$hashed_pw);
        return $user->getAuthPassword() == $hashed_salted_pw;
    }
}