<?php

class ScriptTypesSeeder extends Seeder
{
    /** @noinspection PhpHierarchyChecksInspection */
    public function run()
    {
        DB::table('script_types')->truncate();

        $types = array('Free', 'VIP', 'Premium');

        foreach ($types as $type) {
            ScriptType::create(array('value' => $type));
        }
    }
}
