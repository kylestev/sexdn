<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
    /** @noinspection PhpHierarchyChecksInspection */
    public function run()
	{
		Eloquent::unguard();

		$this->call('CategorySeeder');
		$this->call('ScriptTypesSeeder');
	}

}