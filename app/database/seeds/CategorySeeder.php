<?php

class CategorySeeder extends Seeder {
    /** @noinspection PhpHierarchyChecksInspection */
    public function run()
    {
        DB::table('categories')->truncate();

        $cats = array(
            'Agility', 'Combat', 'Construction', 'Cooking', 'Crafting',
            'Farming', 'Firemaking', 'Fishing', 'Fletching', 'Herblore',
            'Hunter', 'Magic', 'Minigames', 'Mining', 'Misc',
            'Moneymaking', 'Prayer', 'Ranged', 'Runecrafting', 'Slayer',
            'Smithing', 'Thieving', 'Woodcutting'
        );

        foreach ($cats as $cat) {
            Category::create(array('name' => $cat));
        }
    }
}
