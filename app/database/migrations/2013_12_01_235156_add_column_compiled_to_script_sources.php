<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCompiledToScriptSources extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('script_sources', function(Blueprint $table)
        {
            $table->tinyInteger('compiled')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('script_sources', function(Blueprint $table)
        {
            $table->dropColumn('compiled');
        });
	}

}