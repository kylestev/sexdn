<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScriptSourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('script_sources', function(Blueprint $table)
		{
			$table->string('sha', 40);
            $table->integer('script_id');
            $table->string('version', 25);
            $table->string('change_description', 200);
			$table->timestamps();
            $table->primary('sha');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('script_sources');
	}

}
