<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	if (empty($_SERVER['HTTPS']) && App::environment() == 'production') {
        if (str_is('/bot/*', Request::getPathInfo())) {
            return array(
                'error' => 'Use of SSL is required when using the JSON API.'
            );
        }

        return Redirect::secure(Route::currentRouteName());
    }
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest(URL::route('show_login'));
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

Route::filter('script.owner', function($route, $request)
{
    preg_match('/\/manage\/(\d+)\//', $request->getPathInfo(), $matches);
    if (Auth::user()->is_admin()) return;
    if (sizeof($matches) == 2 && Auth::user()->getKey() != Script::find(intval($matches[1]))->user_id) {
        App::abort(403, 'You are not the owner of that script!');
        return 'nope';
    }
});

Route::filter('user.sw', function() {
    if (!(Auth::user()->is_dev() || Auth::user()->is_admin()))
    {
        return App::abort('You need to be a script writer, developer, contributor, or admin to access this page.');
    }
});

Route::filter('user.admin', function()
{
    if (!Auth::user()->is_admin())
    {
        return App::abort(403, 'You are not an admin.');
    }
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});