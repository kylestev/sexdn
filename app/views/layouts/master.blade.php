<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>@yield('title', 'Hexbot SDN')</title>
    <link rel="shortcut icon" href="newfavicon.ico" type="image/x-icon" />
    <link href="//fonts.googleapis.com/css?family=Share|Open+Sans|Nixie+One|Andika" rel="stylesheet" type="text/css" />
    <link href="{{ asset('content/css/style.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="navigate">
        @section('nav')
        @show

        <div class="categories">
@if (Auth::check() && Auth::user()->member_id == 3)
            <a href="#">
                <div class="label">Ranks</div>
            </a>

            <a href="/checkout/ranks/vip.html">
                <div class="category">VIP</div>
            </a>
            <a href="/checkout/ranks/sponsor.html">
                <div class="category">Sponsor</div>
            </a>
            <a href="/checkout/hexpoints/hexpoints.html">
                <div class="category">HexPoints</div>
            </a>
@endif

            <a href="#">
                <div class="label">Script Types</div>
            </a>

            <a href="{{ action('BrowseController@browsePopular') }}">
                <div class="category">Popular</div>
            </a>

            @foreach ($types->filter(function($t) { if ($t->type_id>1) return $t; }) as $type)
            <a href="{{ action('BrowseController@browseType', array('type' => $type->type_id)) }}-{{ strtolower($type->value) }}">
                <div class="category">{{ $type->value }}</div>
            </a>
@endforeach

            <a href="#">
                <div class="label">Categories</div>
            </a>

@foreach ($categories as $category)
            <a href="{{ action('BrowseController@browseCategory', array('cat' => $category->category_id)) }}-{{ strtolower($category->name) }}">
                <div class="category">{{ $category->name }}</div>
            </a>
@endforeach
        </div>
    </div>

    @section('header')
    @show


    @section('content')
    @show

    <!-- load js after page loads since we are nice people -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="{{ asset('content/js/temp.js') }}"></script>
</body>
</html>