<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', 'Hexbot SDN')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('content/bs/css/bootstrap.min.css') }}" rel="stylesheet" />

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <style type="text/css">
            body {
                background-color: #eee;
            }

            #content {
                padding: 40px 15px;
            }
        </style>
    </head>
    <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('browse_popular') }}">Hexbot SDN</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('script.overview') }}">Manage</a></li>
                    <li><a href="{{ route('script.create') }}">Create</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
    <div class="container" id="content">
        @section('content')
        <p>hi</p>
        @show
    </div>
@section('js')
        <script src="//code.jquery.com/jquery.js"></script>
        <script src="{{ asset('content/bs/js/bootstrap.min.js') }}"></script>
@show
    </body>
</html>