@extends('layouts.bs')

@section('content')

            <style>
                body {
                    padding-top: 40px;
                    padding-bottom: 40px;
                    background-color: #eee;
                }

                .form-signin {
                    max-width: 330px;
                    padding: 15px;
                    margin: 0 auto;
                }
                .form-signin .form-signin-heading,
                .form-signin .checkbox {
                    margin-bottom: 10px;
                }
                .form-signin .checkbox {
                    font-weight: normal;
                }
                .form-signin .form-control {
                    position: relative;
                    font-size: 16px;
                    height: auto;
                    padding: 10px;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }
                .form-signin .form-control:focus {
                    z-index: 2;
                }
                .form-signin input[type="text"] {
                    margin-bottom: -1px;
                    border-bottom-left-radius: 0;
                    border-bottom-right-radius: 0;
                }
                .form-signin input[type="password"] {
                    margin-bottom: 10px;
                    border-top-left-radius: 0;
                    border-top-right-radius: 0;
                }
            </style>
            {{ Form::open(array('route' => 'handle_login', 'method' => 'post', 'class' => 'form-signin')) }}

                <h2 class="form-signin-heading">Please sign in</h2>
                {{ Form::text('username', '', array('class' => 'form-control', 'placeholder' => 'Hexbot username', 'required', 'autofocus')) }}

                {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password', 'required')) }}

                {{ Form::button('Sign in', array('class' => 'btn btn-lg btn-green btn-primary btn-block', 'type' => 'submit')) }}

            {{ Form::close() }}

@endsection