@extends('layouts.bs')

@section('content')

        <h2>Create a script</h2>

        {{ Form::open(array('route' => 'script.create_post', 'role' => 'form', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label('name', 'Script Name', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Script name', 'required')) }}

            </div>
        </div>

        <div class="form-group">
            {{ Form::label('type_id', 'Script Type', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::select('type_id', $script_types, null, array('class' => 'form-control', 'required')) }}

            </div>
        </div>

        <div class="form-group">
            {{ Form::label('category_id', 'Category', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::select('category_id', $categories, null, array('class' => 'form-control', 'required')) }}

            </div>
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Description', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::textarea('description', '', array('class' => 'form-control', 'rows' => 5, 'placeholder' => 'Script description', 'required')) }}

            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {{ Form::button('Create', array('type' => 'submit', 'class' => 'btn btn-default')) }}
            </div>
        </div>

        {{ Form::close() }}

@endsection