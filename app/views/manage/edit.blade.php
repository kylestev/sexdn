@extends('layouts.bs')

@section('content')

        <h2>Editing {{{ $script->name }}}</h2>

        {{ Form::model($script, array('route' => array('script.update', $script->getKey()), 'role' => 'form', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label('name', 'Script Name', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::text('name', $script->name, array('class' => 'form-control')) }}

            </div>
        </div>

        <div class="form-group">
            {{ Form::label('type_id', 'Script Type', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::select('type_id', $script_types, $script->type_id, array('class' => 'form-control')) }}

            </div>
        </div>

        <div class="form-group">
            {{ Form::label('category_id', 'Category', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::select('category_id', $categories, $script->category_id, array('class' => 'form-control')) }}

            </div>
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Description', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::textarea('description', $script->description, array('class' => 'form-control', 'rows' => 5)) }}

            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {{ Form::button('Update', array('type' => 'submit', 'class' => 'btn btn-default')) }}
            </div>
        </div>

        {{ Form::close() }}

@endsection