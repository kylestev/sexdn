@extends('layouts.bs')

@section('content')
    <h2>Authored Scripts</h2>

    @if (sizeof($scripts) > 0)
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Name</th>
                <th width="60px">Users</th>
                <th width="60px">Type</th>
                <th width="100px">Category</th>
                <th width="60px">Edit</th>
                <th width="80px">Update</th>
                <th width="80px">Remove</th>
                <th width="80px">View Log</th>
            </tr>
        </thead>
        <tbody>
@foreach ($scripts as $script)
            <tr class="{{ $script->visible == 1 ? 'success' : 'warning' }}">
                <td>{{ link_to_route('script.edit', $script->name, array($script->getKey())) }}</td>
                <td>{{ link_to_route('script.stats', $script->num_users(), array($script->getKey())) }}</td>
                <td>{{ link_to_route('browse_types', $script->type->value, array($script->type->getKey())) }}</td>
                <td>{{ link_to_route('browse_category', $script->category->name, array($script->category->getKey())) }}</td>
                <td>{{ link_to_route('script.edit', 'Edit', array($script->getKey())) }}</td>
                <td>{{ link_to_route('script.upload', 'Update', array($script->getKey())) }}</td>
                <td>
                    @if ($script->type->type_id != 3)
                    {{ Form::open(array('route' => array('script.delete', $script->getKey()), 'method' => 'post')) }}
                    {{ Form::submit('Remove') }}
                    {{ Form::close() }}
                    @endif
                </td>
                <td>
                    @if (file_exists('/home/admin/public_html/logs/'.$script->getKey().'.log'))
                        <a href="/logs/{{ $script->getKey() }}.log">View Log</a>
                    @else
                        No log!
                    @endif
                </td>
            </tr>
@endforeach
        </tbody>
    </table>
    @else
    <h3>Whoa there buddy. You might want to upload some scripts!</h3>
    @endif
@endsection