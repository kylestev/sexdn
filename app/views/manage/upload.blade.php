@extends('layouts.bs')

@section('content')

        <h2>Upload an update to {{{ $script->name }}}</h2>

        {{ Form::open(array('route' => array('script.upload_post', $script->getKey()), 'role' => 'form', 'class' => 'form-horizontal', 'files' => TRUE)) }}

        <div class="form-group">
            {{ Form::label('zip_file', 'Zip File', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::file('zip_file', array('class' => 'form-control', 'placeholder' => 'Script name')) }}

            </div>
        </div>

        <div class="form-group">
            {{ Form::label('version', 'Version', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::text('version', '', array('class' => 'form-control', 'placeholder' => 'Version')) }}

            </div>
        </div>

        <div class="form-group">
            {{ Form::label('change_description', 'Change Description', array('class' => 'col-sm-2 control-label')) }}

            <div class="col-sm-10">
                {{ Form::textarea('change_description', '', array('class' => 'form-control', 'rows' => 5, 'placeholder' => 'What did you change?')) }}

            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {{ Form::button('Upload', array('type' => 'submit', 'class' => 'btn btn-default')) }}
            </div>
        </div>

        {{ Form::close() }}

@endsection