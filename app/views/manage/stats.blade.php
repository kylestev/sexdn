@extends('layouts.bs')

@section('content')

        <h2>{{{ $script->name }}} - Script Stats</h2>

        <p>Users: {{ $script->num_users() }}</p>

@endsection