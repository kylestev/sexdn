@extends('layouts.master')

@section('nav')

        <a href="/">
            <div style="position:absolute; left:0; top:2%; width:100%; text-align:center;">
                <span style="position:absolute; margin-left:-36px; margin-top:-7px">
                    <img src="{{ asset('content/img/logo.png') }}" />
                </span>
                <span style="font-family:'Share', cursive; font-size:20px;">Hexbot</span>
            </div>
        </a>

        <div style="position:absolute; left:50%; top:55px; margin-left:-90px;">
            <input class="search" type="text" placeholder="Search the store" />
        </div>
@stop

@section('header')

    <div class="header">
        <div class="header_user">
            <span class="header_user_name">
                @if (!Auth::check())
                    <a href="{{ route('show_login') }}">sign in</a> |
                    <a href="/forum/index.php?app=core&module=global&section=register">register</a>
                @else
                    <b>{{{ Auth::user()->name }}}</b> | {{ number_format(Auth::user()->points) }} HexPoints |
                            <a href="{{ route('my_scripts') }}">my scripts</a> |
                            <a href="{{ route('script.overview') }}">manage</a> |
                            <a href="{{ route('auth_logout') }}">logout</a>
                @endif
                | <a href="/forum/">forum</a> | <a href="/">home</a>
            </span>
        </div>
    </div>
@stop