@extends('store.main')

@section('content')

        <div class="scripts">
            <div id="thanks">Thanks!</div>
            <div id="featured">
                <div class="content">
                    <span id="header">@yield('script_type', 'Unknown')</span>
                </div>
            </div>

            <div id="standard">
@if (sizeof($scripts) == 0)
                <!--<script type="text/javascript">
                    alert("Looks like there are no scripts of this type.");
                </script>-->
@else
                @foreach ($scripts as $script)
                <?php
                if ($script->visible == 0) continue;

                $status = $script->get_status();
                $onclick = array('onclick' => sprintf('document.getElementById("s%d").submit();', $script->id));

                switch ($status) {
                    case SCRIPT_STATUS_GUEST:
                        $btn_bg = '#BDBDBD';
                        $link = link_to('#', 'LOG IN');
                        $_action = '';
                        break;
                    case SCRIPT_STATUS_UNADDED:
                        $btn_bg = '#58ACFA';
                        $link = link_to('#', 'ADD', $onclick);
                        $_action = route('add_script', array($script->id));
                        break;
                    case SCRIPT_STATUS_ADDED:
                        $btn_bg = '#FF3333';
                        $link = link_to('#', 'REMOVE', $onclick);
                        $_action = route('remove_script', array($script->id));
                        break;
                    case SCRIPT_STATUS_VIP_NEEDED:
                        $btn_bg = '#626262';
                        $link = link_to('#', 'VIP ONLY');
                        $_action = '';
                        break;
                    case SCRIPT_STATUS_PURCHASE_NEEDED:
                        $btn_bg = '#D1D1D1';
                        $link = link_to('#', 'BUY');
                        $_action = '';
                        break;
                }
                ?>

                <div id="{{ $script->script_id }}" class="basic_script" style="background: #E6E6E6">
                    <div class="basic_script_display">
                        <div class="basic_script_label {{ strtolower($script->type->value) }}">{{ strtoupper($script->type->value) }}</div>
                        <div class="basic_script_img" style="background:url(//hexbot.org/sdn/content/img/scripts/{{ $script->category->name }}.png); background-size: 220px 140px;"></div>
                        <div class="basic_script_desc">{{{ $script->name }}}</div>
                        <div class="basic_script_users">{{ $script->num_users() }} users</div>
                    </div>
                    <div class="basic_script_info">
                        <div class='basic_script_info_title' style="">
                            {{{ $script->name }}} <br />by
                            <a href="http://hexbot.org/forum/index.php?/user/{{$script->author->member_id}}-{{{$script->author->name}}}">
                                {{ $script->author->get_whore_name() }}
                            </a>
                        </div>
                        <div class="basic_script_info_icon" style="background:url(//hexbot.org/sdn/content/img/scripts/{{ $script->category->name }}-icon.png); background-size: 50px 50px;"></div>
                        <div class='basic_script_info_desc'>
                            {{{ $script->description }}}
                        </div>
                        <div id="{{ $script->script_id }}" class="basic_script_info_button" style="background:{{ $btn_bg }}">
                            <form id="s{{ $script->id }}" method="post" action="{{ $_action }}">
                                {{ $link }}
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
@endif
            </div>
            <div id="loading"><img style="padding:8px 8px;" src="{{ asset('content/img/load.gif') }}" /></div>
            <div id="error"><span style="padding: 18px 28px;">Error loading scripts</span></div>
        </div>
@endsection