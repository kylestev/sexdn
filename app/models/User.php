<?php

use \Illuminate\Auth\UserInterface;
use \Illuminate\Support\Collection;

define('USER_GROUP_VIP', 14);
define('USER_GROUP_SPONSOR', 7);

define('USER_GROUP_ADMIN', 4);
define('USER_GROUP_HIDDEN_STAFF', 13);

define('USER_GROUP_DEVELOPER', 12);
define('USER_GROUP_CONTRIBUTOR', 10);
define('USER_GROUP_SCRIPT_WRITER', 9);

class User extends Eloquent implements UserInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'members';

    protected $primaryKey = 'member_id';

    protected $g_admin = array(
        USER_GROUP_ADMIN,
        USER_GROUP_HIDDEN_STAFF
    );

    protected $g_dev = array(
        USER_GROUP_DEVELOPER,
        USER_GROUP_SCRIPT_WRITER,
        USER_GROUP_CONTRIBUTOR
    );

    protected $g_vip = array(
        USER_GROUP_VIP,
        USER_GROUP_SPONSOR
    );

    protected $_groups = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

    public function scripts_authored() {
        $script_ids = array();
        $script_users = DB::table('scripts')->where('user_id', $this->member_id);
        foreach ($script_users->get() as $script_user) {
            $script_ids []= $script_user->id;
        }

        $scripts = array();
        foreach ($script_ids as $script_id) {
            $script = Script::find($script_id);

            if (is_null($script) || $script->id != $script_id) {
                continue;
            }

            $scripts []= $script;
        }

        return new Collection($scripts);
    }

    public function get_scripts() {
        $script_ids = array();
        $script_users = DB::table('script_users')->where('user_id', $this->member_id);

        foreach ($script_users->get() as $script_user) {
            $script_ids []= $script_user->script_id;
        }

        $scripts = array();
        foreach ($script_ids as $script_id) {
            $script = Script::find($script_id);

            if (is_null($script) || $script->id != $script_id) {
                continue;
            }

            $scripts []= $script;
        }

        return new Collection($scripts);
    }

    public function get_json_scripts() {
        $script_ids = array();
        foreach (DB::table('script_users')->where('user_id', $this->getKey())->get() as $script_user) {
            $script_ids []= $script_user->script_id;
        }

        $details = array();
        foreach (Script::whereIn('id', $script_ids)->with('author', 'category', 'type')->get() as $script) {
            if ($script->visible != 1) continue;
            $details []= array(
                'id' => $script->getKey(),
                'name' => $script->name,
                'type' => $script->type->getKey(),
                'author' => $script->author->name,
                'category' => $script->category->name,
                'description' => $script->description
            );
        }

        return new Collection($details);
    }

    public function groups() {
        if (sizeof($this->_groups) == 0) {
            $this->_groups []= $this->member_group_id;
            $split = explode(',', $this->mgroup_others);

            foreach ($split as $s) {
                $this->_groups []= intval($s);
            }
        }

        return $this->_groups;
    }

    /**
     * @param int $script_id
     * @return bool
     */
    public function add_script($script_id) {
        $script = Script::findOrFail($script_id);
        $status = $script->get_status($this);

        switch ($status) {
            case SCRIPT_STATUS_UNADDED:
                break;
            case SCRIPT_STATUS_GUEST:
            case SCRIPT_STATUS_ADDED:
            case SCRIPT_STATUS_VIP_NEEDED:
            case SCRIPT_STATUS_PURCHASE_NEEDED:
                return FALSE;
        }

        DB::table('script_users')->insert(array(
            'script_id' => $script_id,
            'user_id' => $this->member_id
        ));
    }

    public function remove_script($script_id) {
        DB::table('script_users')
                ->where('user_id', $this->member_id)
                ->where('script_id', $script_id)
                ->delete();
    }

    /**
     * Determines if this group is a VIP type
     * @return bool
     */
    public function is_vip() {
        return $this->_in_group($this->g_vip) || $this->_in_group($this->g_admin);
    }

    /**
     * @return bool
     */
    public function is_dev() {
        return $this->_in_group($this->g_dev);
    }

    /**
     * @return bool
     */
    public function is_admin() {
        return $this->_in_group($this->g_admin);
    }

    /**
     * @param $group_ids
     * @return bool
     */
    private function _in_group($group_ids) {
        $intersection = array_intersect($this->groups(), $group_ids);
        return sizeof($intersection) > 0;
    }

    public function has_paid_for($script_id) {
        return DB::table('script_purchases')
                ->where('script_id', $script_id)
                ->where('user_id', $this->member_id)
                ->count() > 0;
    }

    public function has_added($script_id) {
        return DB::table('script_users')
            ->where('script_id', $script_id)
            ->where('user_id', $this->member_id)
            ->count() > 0;
    }

    public function get_whore_name() {
        // Caches for 1 day (1440 minutes) so if someone changes in rank, they don't bitch about
        // not having new whore name in the SDN
        return Cache::remember("wn{$this->member_id}", 1440, function() {
            $title = $this->name;
            foreach (Group::get_cache() as $group) {
                if ($group->g_id == $this->member_group_id) {
                    return sprintf('%s%s%s', $group->prefix, $this->name, $group->suffix);
                }
            }
            return $title;
        });
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return int
     */
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword() {
        return $this->members_pass_hash;
    }

    public function getPasswordSalt() {
        return md5($this->members_pass_salt);
    }
}