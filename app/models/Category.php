<?php
/**
 * Created by JetBrains PhpStorm.
 * User: kyle
 * Date: 11/9/13
 * Time: 10:22 PM
 * To change this template use File | Settings | File Templates.
 */

class Category extends Eloquent {
    protected $table = 'categories';
    protected $primaryKey = 'category_id';
    public $timestamps = false;

    public function scripts() {
        return $this->hasMany('Script', 'category_id');
    }

    public static function get_cache() {
        return Cache::rememberForever('categories', function() {
            return Category::all();
        });
    }

    public static function get_mapped_cache() {
        return Cache::rememberForever('categories_mapped', function() {
            $categories = array();

            foreach (self::get_cache() as $cat) {
                $categories[$cat->category_id] = $cat->name;
            }

            ksort($categories);

            return $categories;
        });
    }
}