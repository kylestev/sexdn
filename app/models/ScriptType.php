<?php
/**
 * Created by JetBrains PhpStorm.
 * User: kyle
 * Date: 11/9/13
 * Time: 10:49 PM
 * To change this template use File | Settings | File Templates.
 */

class ScriptType extends Eloquent {
    protected $table = 'script_types';
    protected $primaryKey = 'type_id';
    public $timestamps = false;

    public function scripts() {
        return $this->hasMany('Script', 'type_id');
    }

    public static function get_cache() {
        return Cache::rememberForever('script_types', function() {
            return ScriptType::all();
        });
    }

    public static function get_mapped_cache() {
        return Cache::rememberForever('script_types_mapped', function() {
            $types = array();

            foreach (self::get_cache() as $stype) {
                $types[$stype->type_id] = $stype->value;
            }

            return $types;
        });
    }
}