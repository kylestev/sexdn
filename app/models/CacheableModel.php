<?php
/**
 * Created by JetBrains PhpStorm.
 * User: kyle
 * Date: 11/17/13
 * Time: 9:51 AM
 * To change this template use File | Settings | File Templates.
 */

interface CacheableModel {
    public static function get_cache_length();
}