<?php

class ScriptSource extends Eloquent {
    protected $primaryKey = 'sha';
    protected $table = 'script_sources';
    protected $fillable = array('sha', 'script_id', 'version', 'change_description');

    public function script() {
        return $this->belongsTo('Script', 'script_id', 'script_id');
    }
}
