<?php
/**
 * Created by JetBrains PhpStorm.
 * User: kyle
 * Date: 11/9/13
 * Time: 10:13 PM
 * To change this template use File | Settings | File Templates.
 */

define('SCRIPT_STATUS_GUEST', 0);
define('SCRIPT_STATUS_UNADDED', 1);
define('SCRIPT_STATUS_ADDED', 2);
define('SCRIPT_STATUS_VIP_NEEDED', 3);
define('SCRIPT_STATUS_PURCHASE_NEEDED', 4);

class Script extends Eloquent {
    protected $table = 'scripts';
    protected $fillable = array('name', 'description', 'type_id', 'category_id', 'user_id');

    /**
     * @return User
     */
    public function author() {
        return $this->belongsTo('User', 'user_id', 'member_id');
    }

    /**
     * @return Category
     */
    public function category() {
        return $this->belongsTo('Category', 'category_id');
    }

    /**
     * @return ScriptType
     */
    public function type() {
        return $this->belongsTo('ScriptType', 'type_id');
    }

    public function is_premium() {
        return $this->type->value == 'Pro';
    }

    public function is_vip() {
        return $this->type->value == 'VIP';
    }

    public function get_status() {
        if (!Auth::check()) {
            return SCRIPT_STATUS_GUEST;
        }

        $user = Auth::user();

        if ($user->name == 'Guest') {
            return SCRIPT_STATUS_GUEST;
        } elseif ($user->has_added($this->id)) {
            return SCRIPT_STATUS_ADDED;
        } elseif ($this->is_vip() && !$user->is_vip()) {
            return SCRIPT_STATUS_VIP_NEEDED;
        } elseif ($this->is_premium() && !$user->has_paid_for($this->id)) {
            return SCRIPT_STATUS_PURCHASE_NEEDED;
        }

        return SCRIPT_STATUS_UNADDED;
    }

    /**
     * Kind of retarded... Since it sorts in ascending order, and we want descending, we negate the number of users
     * to get the results without having to reverse the whole array and deal with all that memory leakage.
     * @return \Illuminate\Support\Collection
     */
    public static function popular() {
        if (!Cache::has('popular_scripts')) {
            $top = Script::with('category', 'type', 'author')->get()->filter(function(Script $script) {
                if ($script->attributes['visible'] == 1) {
                    return $script;
                }
            })->sortBy(function(Script $script) {
                //return -$script->num_users() - ($script->is_premium() ? 1000000 : 0); #vidallogic
                return -$script->num_users();
            })->take(15);

            Cache::put('popular_scripts', $top, self::get_cache_length());

            return $top;
        }

        return Cache::get('popular_scripts');
    }

    public function num_users() {
        assert(is_integer($this->id));
        return Cache::remember("_su{$this->id}", self::get_cache_length(), function() {
            if ($this->n > 0) {
                return $this->n;
            }
            return DB::table('script_users')
                    ->where('script_id', '=', $this->id)
                    ->count();
        });
    }

    /**
     * @return int time in minutes to cache results for
     */
    protected static function get_cache_length() {
        return 30;
    }
}