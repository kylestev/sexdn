<?php
/**
 * Created by JetBrains PhpStorm.
 * User: kyle
 * Date: 11/17/13
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */


class Group extends Eloquent {
    protected $table = 'groups';

    protected $primaryKey = 'g_id';

    public static function get_cache() {
        return Cache::rememberForever('groups', function() {
            return Group::all();
        });
    }
}