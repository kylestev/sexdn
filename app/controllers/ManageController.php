<?php
/**
 * Created by JetBrains PhpStorm.
 * User: kyle
 * Date: 11/17/13
 * Time: 7:40 PM
 * To change this template use File | Settings | File Templates.
 */

class ManageController extends Controller {
    public function showIndex() {
        return View::make('manage.overview', array(
            'user' => Auth::user(),
            'scripts' => Auth::user()->scripts_authored()
        ));
    }

    public function showStats(Script $script) {
        return View::make('manage.stats', array(
            'user' => Auth::user(),
            'script' => $script
        ));
    }

    public function displayUpload(Script $script) {
        return View::make('manage.upload', array(
            'user' => Auth::user(),
            'script' => $script
        ));
    }

    public function uploadUpdate(Script $script) {
        $zip = Input::file('zip_file');
        $sha = sha1_file($zip->getRealPath());
        $dupe = ScriptSource::find($sha);

        if (!is_null($dupe) && $dupe->sha == $sha) {
            App::abort(400, 'Aww shucks. That has already been uploaded before.');
        } elseif (!is_null(DB::table('script_sources')->where('script_id', $script->getKey())->where('version', Input::get('version'))->first())) {
            App::abort(400, 'Aww shucks. That version has already been uploaded before.');
        }

        $zip->move(storage_path() . '/scripts/src/', $sha . '.zip');

        $data['sha'] = $sha;
        $data['script_id'] = $script->getKey();
        $data['version'] = Input::get('version');
        $data['change_description'] = Input::get('change_description');

        $source = ScriptSource::create($data);
        $source->save();

        return Redirect::route('script.overview');
    }

    private function updateCategoryId(Script $script, $cat_id) {
        if (Category::get_cache()->contains($cat_id)) {
            $script->category_id = $cat_id;
        }
    }

    private function updateTypeId(Script $script, $type_id) {
        if (ScriptType::get_cache()->contains($type_id)) {
            $script->type_id = $type_id;
        }
    }

    public function showScriptCreate() {
        return View::make('manage.create', array(
            'user' => Auth::user(),
            'categories' => Category::get_mapped_cache(),
            'script_types' => ScriptType::get_mapped_cache()
        ));
    }

    public function createScript() {
        $attrs = array(
            'name' => Input::get('name'),
            'description' => Input::get('description'),
            'user_id' => Auth::user()->getKey()
        );

        $script = Script::create($attrs);
        $this->updateCategoryId($script, Input::get('category_id'));
        $this->updateTypeId($script, Input::get('type_id'));
        $script->save();

        return Redirect::route('script.edit', $script->getKey());
    }

    public function editScript(Script $script) {
        return View::make('manage.edit', array(
            'user' => Auth::user(),
            'script' => $script,
            'categories' => Category::get_mapped_cache(),
            'script_types' => ScriptType::get_mapped_cache()
        ));
    }

    public function updateScript(Script $script) {
        $script->name = Input::get('name');
        $script->description = Input::get('description');

        $this->updateCategoryId($script, Input::get('category_id'));
        $this->updateTypeId($script, Input::get('type_id'));

        $script->save();

        return Redirect::route('script.edit', $script->getKey());
    }

    public function removeScript(Script $script) {
        DB::table('script_users')->where('script_id', $script->getKey())
            ->delete();
        $script->delete();

        return Redirect::route('script.overview');
    }
}