<?php
/**
 * Created by JetBrains PhpStorm.
 * User: kyle
 * Date: 11/10/13
 * Time: 1:23 AM
 * To change this template use File | Settings | File Templates.
 */

class ScriptsController extends Controller {
    protected $layout = '';

    private function smart_redirect() {
        $ref = $_SERVER['HTTP_REFERER'];
        $split = explode('/', $ref);

        if ($split[3] == 'sdn') {
            return Redirect::to($ref);
        }

        return Redirect::route('my_scripts');
    }

    public function add(Script $script) {
        $user = Auth::user();
        $user->add_script($script->id);
        return $this->smart_redirect();
    }

    public function remove(Script $script) {
        $user = Auth::user();
        $user->remove_script($script->id);
        return $this->smart_redirect();
    }
}