<?php

class LoginController extends Controller {
    public function showLogin() {
        return View::make('login');
    }

    public function handleLogin() {
        if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')), TRUE)) {
            return Redirect::intended();
        }

        return Redirect::route('show_login');
    }

    public function handleLogout() {
        Auth::logout();
        return Redirect::route('browse_popular');
    }
}