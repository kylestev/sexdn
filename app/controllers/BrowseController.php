<?php
/**
 * Created by JetBrains PhpStorm.
 * User: kyle
 * Date: 11/9/13
 * Time: 11:49 PM
 * To change this template use File | Settings | File Templates.
 */

class BrowseController extends Controller {
    protected $layout = 'store.main';

    public function browseType(ScriptType $type) {
        return View::make('store.type', array(
            'scripts' => Script::where('type_id', '=', $type->type_id)->paginate(25),
            'type' => $type
        ));
    }

    public function browseCategory(Category $cat) {
        return View::make('store.category', array(
            'scripts' => Script::where('category_id', '=', $cat->category_id)->orderBy('type_id', 'desc')->get(),
            'category' => $cat
        ));
    }

    public function browseMine() {
        return View::make('store.mine', array(
            'scripts' => Auth::user()->get_scripts()
        ));
    }

    public function browseAuthored() {
        return View::make('store.mine', array(
            'scripts' => Auth::user()->scripts_authored()
        ));
    }

    public function browsePopular() {
        return View::make('store.popular', array(
            'scripts' => Script::popular()
        ));
    }
}