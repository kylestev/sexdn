<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'BrowseController@browsePopular');

Route::get('/env', function() {
    return App::environment();
});

View::composer('store.*', function($view) {
    $view->with('types', ScriptType::get_cache()->sortBy(function($type) {
        return $type->type_id;
    }));
    $view->with('categories', Category::get_cache());
});

View::composer('layouts.scripts', function($view) {
});

function check_auth() {
    $pw_hash = Input::get('pw_hash');
    $username = Input::get('username');

    if (!Input::has('pw_hash') || !Input::has('username')) {
        $res['error'] = 'Looks like you forgot a param or two.';

        return $res;
    }

    $details = DB::table('members')->where('name', $username)
                    ->select(array('members_pass_hash', 'members_pass_salt',
                                   'member_group_id', 'member_id'))->first();

    $hashed = md5(md5($details->members_pass_salt) . $pw_hash);
    $authenticated = (bool) ($hashed == $details->members_pass_hash);

    $res['authenticated'] = $authenticated;

    if (!$authenticated) {
        $res['error'] = 'Cannot authenticate.';
        return $res;
    }

    $res['uid'] = $details->member_id;
    $res['gid'] = $details->member_group_id;

    return $res;
}

// auth
Route::get('/bot/scripts', array(
    'as' => 'bot_scripts',
    function() {
        $res = check_auth();
        if (array_key_exists('error', $res)) {
            return $res;
        }

        $fetch_scripts = is_null(Input::get('auth_only'));

        if ($fetch_scripts) {
            $res['scripts'] = User::find($res['uid'])->get_json_scripts()->toArray();
        }

        return $res;
    })
);

Route::get('/bot/scripts/{script}/jar', array(
    'as' => 'script_jar',
    function(Script $script) {
        $res = check_auth();
        if (array_key_exists('error', $res)) {
            return $res;
        }

        $user_id = $res['uid'];
        $sha = DB::table('script_sources')->where('compiled', 1)
                ->where('script_id', $script->getKey())
                ->orderBy('created_at', 'desc')->first()->sha;
        $file_path = storage_path() . '/scripts/obbed/' . $sha . '.jar';

        if (!file_exists($file_path)) {
            return array(
                'error' => 'Script jar did not exist on the server...'
            );
        }

        $headers['Expires'] = '0';
        $headers['Pragma'] = 'public';
        $headers['Connection'] = 'close';
        $headers['Content-Transfer-Encoding'] = 'binary';
        $headers['Content-Type'] = 'application/java-archive';
        $headers['Cache-Control'] = 'must-revalidate, post-check=0, pre-check=0';
        $headers['Content-Disposition'] = sprintf('attachment; filename="%s.jar"', $script->getKey());

        $response = Response::make(readfile($file_path), 200);
        foreach ($headers as $key => $value) {
            $response->header($key, $value);
        }

        return $response;
    })
);

Route::get('/_compiler', function() {
    $scripts_path = storage_path() . '/scripts';
    $scripts_bin = $scripts_path . '/bin';
    $scripts_src = $scripts_path . '/src';
    $scripts_logs = $scripts_path . '/logs';

    foreach (ScriptSource::where('compiled', '=', '0') as $source) {
        if ($source->compiled != 0) continue;
        if (!file_exists($scripts_src . "/{$source->sha}.zip")) {
            echo "skipping {$scripts_src}/{$source->sha}.zip\n";
            //continue;
        }

        if (is_dir($scripts_bin . "/{$source->sha}")) {
            shell_exec("rm -rf {$scripts_bin}/{$source->sha}");
        }

        if (is_dir($scripts_src . "/{$source->sha}")) {
            shell_exec("rm -rf {$scripts_src}/{$source->sha}");
        }

        $result = shell_exec('/home/admin/public_html/sdn/ant/bin/ant -buildfile build.xml -Dsid=' . $source->sha . ' -Didx='.$source->script_id.' 2>&1');
        //file_put_contents("{$scripts_logs}/{$source->script_id}-{$source->sha}.txt", $result);

        if (strpos($result, "BUILD SUCCESSFUL") > 0) {
            $source->compiled = 1;
            $source->save();

            $script = Script::find($source->script_id);
            $script->visible = 1;
            $script->save();
        } else {
            // failed
            echo "{$source->script_id}-{$source->sha} FAILED\n<br />\n";
            $source->compiled = -1;
            $source->save();
        }
    }
});

Route::group(array('prefix' => '/auth'), function() {
    Route::get('/login', array(
        'as' => 'show_login',
        'uses' => 'LoginController@showLogin'
    ));

    Route::post('/login', array(
        'as' => 'handle_login',
        'uses' => 'LoginController@handleLogin'
    ));

    Route::get('/logout', array(
        'as' => 'auth_logout',
        'before' => 'auth',
        'uses' => 'LoginController@handleLogout'
    ));

    Route::get('/', function() {
        return Redirect::route('show_login');
    });
});

/**
 * SCRIPTS
 */
Route::model('script', 'Script');
Route::group(array('prefix' => '/scripts'), function() {
    Route::get('/', array(
        'as' => 'scripts_dashboard',
        function() {
            return View::make('scripts.dashboard');
        }
    ));

    Route::post('/{script}/add', array(
        'as' => 'add_script',
        'before' => 'auth',
        'uses' => 'ScriptsController@add'
    ));

    Route::post('/{script}/remove', array(
        'as' => 'remove_script',
        'before' => 'auth',
        'uses' => 'ScriptsController@remove'
    ));

    Route::get('/{script}/create', array(
        'as' => 'create_script',
        'before' => 'auth',
        'uses' => 'ScriptsController@upload'
    ));
});

Route::group(array('prefix' => '/manage', 'before' => 'auth'), function() {
    Route::get('/', array(
        'as' => 'script.overview',
        'before' => 'user.sw',
        'uses' => 'ManageController@showIndex'
    ));

    Route::get('/{script}/stats', array(
        'as' => 'script.stats',
        'before' => 'script.owner|user.sw',
        'uses' => 'ManageController@showStats'
    ));

    Route::get('/{script}/edit', array(
        'as' => 'script.edit',
        'before' => 'script.owner|user.sw',
        'uses' => 'ManageController@editScript'
    ));

    Route::post('/{script}/update', array(
        'as' => 'script.update',
        'before' => 'script.owner|user.sw',
        'uses' => 'ManageController@updateScript'
    ));

    Route::post('/{script}/delete', array(
        'as' => 'script.delete',
        'before' => 'script.owner|user.sw',
        'uses' => 'ManageController@removeScript'
    ));

    Route::get('/{script}/upload', array(
        'as' => 'script.upload',
        'before' => 'script.owner|user.sw',
        'uses' => 'ManageController@displayUpload'
    ));

    Route::post('/{script}/upload', array(
        'as' => 'script.upload_post',
        'before' => 'script.owner|user.sw',
        'uses' => 'ManageController@uploadUpdate'
    ));

    Route::get('/create', array(
        'as' => 'script.create',
        'before' => 'user.sw',
        'uses' => 'ManageController@showScriptCreate'
    ));

    Route::post('/create/post', array(
        'as' => 'script.create_post',
        'before' => 'user.sw',
        'uses' => 'ManageController@createScript'
    ));
});

Route::group(array('prefix' => '/browse'), function() {
    Route::get('/mine', array(
        'as' => 'my_scripts',
        'before' => 'auth',
        'uses' => 'BrowseController@browseMine'
    ));

    Route::get('/authored', array(
        'as' => 'authored_scripts',
        'before' => 'auth',
        'uses' => 'BrowseController@browseAuthored'
    ));

    /**
     * TYPES
     */
    Route::model('type', 'ScriptType');
    Route::get('/types/{type}', array(
        'as' => 'browse_types',
        'uses' => 'BrowseController@browseType'
    ));

    /**
     * CATEGORIES
     */
    Route::model('cat', 'Category');
    Route::get('/category/{cat}', array(
        'as' => 'browse_category',
        'uses' => 'BrowseController@browseCategory'
    ));

    /**
     * POPULAR
     */
    Route::get('/popular', array(
        'as' => 'browse_popular',
        'uses' => 'BrowseController@browsePopular'
    ));
});

Route::get('/', 'BrowseController@browsePopular');
